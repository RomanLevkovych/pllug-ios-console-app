//
//  Library.swift
//  pllugiOSswiftConsoleApp
//
//  Created by Roman on 11/2/18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

struct HistoryEntity {
    let date: Date
    let owner: String
}

class Library {
    let fond: [Book: [HistoryEntity]]
    let name: String
    
    init(library name: String) {
        self.fond = [Book: [HistoryEntity]]()
        self.name = name
    }
}
