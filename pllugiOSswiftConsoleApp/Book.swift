//
//  Book.swift
//  pllugiOSswiftConsoleApp
//
//  Created by Roman on 11/2/18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

enum Genres {
    case Empty
    case SciFi
    case Fantasy
    case Mythology
    case Adventure
    case Mystery
    case Drama
    case Romance
    case Satire
    case Horror
    case TragicComedy
    case YoungAdultFiction
    case Dystopia
    case ActionThriller
}

class Book {
    let name: String
    let author: String
    let genre: Genres
    let publisher: String
    
    init() {
        self.name = ""
        self.author = ""
        self.genre = .Empty
        self.publisher = ""
    }
    
    init(name: String, author: String, genre: Genres, publisher: String) {
        self.name = name
        self.author = author
        self.genre = genre
        self.publisher = publisher
    }
    
}
